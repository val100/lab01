# Ansible - Workshop
Lab 01: Install Ansible and configure managed node

---

# Instructions

 - Install ansible on ubuntu 18.04
 - Create ssh key and copy to remote machine
 - Configure remote machine to enable ansible to run it.

---

## Install ansible on ubuntu 18.04
```
$ ssh (username)@(control machine ip)
$ sudo apt install ansible
$ sudo apt update
$ sudo apt upgrade
```


## Create ssh key and copy to remote machine
```
$ ssh-keygen
$ ssh-copy-id -i (username)@(node machine ip)
```


## Configure remote machine to enable ansible to run it.
```
$ ssh (username)@(node machine ip)
$ sudo apt upgrade
$ sudo apt install python
```

## Change the file value for pubkeyAuthentication to "yes"
```
$ sudo vim /etc/ssh/sshd_config
press i
```

## Search for the pubkeyAuthentication attribute and change its value from no to yes
```
#LoginGraceTime 2m
#PermitRootLogin prohibit-password
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

PubkeyAuthentication no

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile     .ssh/authorized_keys .ssh/authorized_keys2

#AuthorizedPrincipalsFile none'
```

## Save changes
```
Press esc
press :wq
```

## Restart the ssh service and exit to control machine
```
$ sudo service ssh restart
$ sudo systemctl reload sshd
$ exit
```

## Edit the hosts file

### Open hosts file in editor
```
$ sudo vim /etc/ansible/hosts
press i
```
### Add the following entry - instead of (nodeip) enter the node ip (without brackets) you recieved - for example - 146.148.127.157
```
(nodeips)
```

### Save entry and exit to terminal
```
press esc
press :wq
```

## Ensure valid connection between control machine and managed node machine
```
$ ansible -m ping all
``` 

The result should be as : 

(node machine ip) | SUCCESS => {
    "changed": false,
    "ping": "pong"
}